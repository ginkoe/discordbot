import Discord, { Message } from 'discord.js';
import { GreetCommand } from './commands/GreetCommand';
import { cmdParser, NeonCommand } from './lib/CmdParser';
import Essentials from './config';

const client = new Discord.Client();

client.on('ready', () => {
    console.log('Neon is online.');
});

client.on('message', (msg: Message) => {
    if (msg.content.startsWith(Essentials.prefix)) {
        let cmnd: NeonCommand = cmdParser(msg.content.toLowerCase());
        Essentials.commands.forEach((command) => {
            if (command.name == cmnd.name) command.exec(msg);
        });
    }
});

client.login(Essentials.token);
