export interface NeonCommand {
    name: string;
    param?: string;
    arguments?: string[];
}

export function cmdParser(msg: string): NeonCommand {
    let cmndArray: string[] = msg.slice(1).split(' ');
    return {
        name: cmndArray[0],
        param: cmndArray[1],
        arguments: cmndArray.slice(2),
    };
}
