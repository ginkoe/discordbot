import Discord, { Message } from 'discord.js';

export abstract class Commands {
    abstract name: string;
    abstract description: string;
    abstract exec(msg: Message): void;
}
//
