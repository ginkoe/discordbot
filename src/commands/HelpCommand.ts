import { Message } from 'discord.js';
import { Commands } from '../lib/Command';
import Essentials from '../config';

export class HelpCommand extends Commands {
    name = 'help';
    description = 'Show list of commands.';

    exec(msg: Message) {
        let resultMsg = '**List of Commands:**\n';
        Essentials.commands.forEach((command) => {
            resultMsg += `**${command.name}**: ${command.description}\n`;
        });
        return msg.channel.send(resultMsg);
    }
}
